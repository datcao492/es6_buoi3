const layThongTinTuForm = () => {
  let taskName = document.getElementById("newTask").value;

  let status = false;
  let taskList = new taskList(taskName, status);
  return taskList;
};

const renderToDotaskList = (list) => {
  let contentToDoHTML = "";
  let contentComplteHTML = "";

  list.forEach((item) => {
    if (!item.status) {
      let contentTr = `<li>
        <span>${item.taskName}</span> 
        <div>
          <span onclick="deleteTask('${item.taskName}')"><i class="fa fa-trash-alt"></i></span>
          <span onclick="CompleteTask('${item.taskName}',${item.status})"><i class="fa fa-check-circle"></i></span>
        </div>
      </li>`;
      contentToDoHTML += contentTr;
    } else {
      let contentTr = `<li>
        <span>${item.taskName}</span>   
        <div>
          <span onclick="deleteTask('${item.taskName}')"><i class="fa fa-trash-alt"></i></span>
          <span><i class="fa fa-check-circle" ></i></span>
        </div>
      </li>`;
      contentComplteHTML += contentTr;
    }
  });

  document.getElementById("todo").innerHTML = contentToDoHTML;
  document.getElementById("completed").innerHTML = contentComplteHTML;
};

const timVitri = (taskName, array) => {
  return array.findIndex((item) => {
    return item.taskName == taskName;
  });
};
