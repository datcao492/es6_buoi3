let taskList = [];

const taskList_LOCALSTORAGE = "taskList_LOCALSTORAGE";
const luuLocalStorage = () => {
  let taskListJson = JSON.stringify(taskList);
  localStorage.setItem(taskList_LOCALSTORAGE, taskListJson);
};

var taskListJson = localStorage.getItem(taskList_LOCALSTORAGE);

if (taskListJson) {
  taskList = JSON.parse(taskListJson);
  taskList = taskList.map((item) => {
    return new taskList(item.taskName, item.status);
  });
  renderToDoTaskList(taskList);
}

let validator = new ValidatorTask();
//them task
document.getElementById("addItem").addEventListener("click", () => {
  let newTask = layThongTinTuForm();
  let valid = validator.kiemTraRong("newTask", "taskSpan");

  if (valid) {
    taskList.push(newTask);

    renderToDoTaskList(taskList);

    luuLocalStorage();
    document.getElementById("newTask").value = "";
  }
});

//xoa task
const deleteTask = (taskName) => {
  let viTri = timVitri(taskName, taskList);
  taskList.splice(viTri, 1);

  renderToDoTaskList(taskList);
  luuLocalStorage();
};

//hoan thanh task
const CompleteTask = (taskName, status) => {
  let viTri = timVitri(taskName, taskList);
  taskList[viTri].status = !status;

  renderToDoTaskList(taskList);
  luuLocalStorage();
};

//Sorting A-Z
document.getElementById("two").addEventListener("click", () => {
  taskList.sort((a, b) => {
    return a.taskName.localeCompare(b.taskName);
  });
  renderToDotaskList(taskList);
});

//Sorting Z-A
document.getElementById("three").addEventListener("click", () => {
  taskList.sort((a, b) => {
    return b.taskName.localeCompare(a.taskName);
  });
  renderToDoTaskList(taskList);
});

//All Completed tasks
document.getElementById("one").addEventListener("click", () => {
  let UncompletedTasks = (document.getElementById("todo").style.display =
    "none");
  document.getElementById("completed").classList.toggle(UncompletedTasks);
});
